import axios from 'axios';

export default {
    namespaced: true,
    state: {
        token : '',
        user    : {},
    },
    mutations: {
        setToken: (state, payload) => {
            state.token  = payload
        },
        setUser: (state, payload) => {
            state.user  = payload
        },
    },
    actions: {
        setToken: ({commit}, payload) => {
            
            commit('setToken', payload)


        },
      
        checkToken: ({commit} , payload) => {

         
         let config = {
          method: 'post', 
          url: 'https://localhost:8000/api/auth/me',
          headers: {
           'Authorization': 'Bearer ' + payload,
          },
         }
         
         axios(config)
          .then((response) => {
               commit('setUser', response.data)
          })
          .catch(() => {
              commit('setUser', {})
              commit('setToken', '')
          })
        },

        setUser: ({commit }, payload) => {
            
            commit('setUser', payload)

        },
    },
    getters: {
        user  : state => state.user,
        token : state => state.token,
        guest : state => Object.keys(state.user).length === 0,
    }
}
