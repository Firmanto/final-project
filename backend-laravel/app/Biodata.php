<?php

namespace App;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    public $table = "biodata";
    protected $fillable = ['nama', 'umur', 'tempat_lahir', 'tanggal_lahir', 'alamat', 'golongan_darah', 'pekerjaan', 'email', 'foto', 'created_by', 'updated_by'];
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot() {
        parent::boot();
        static::creating(function($model) {
            if(empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
    
}