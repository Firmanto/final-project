## About Final Project

1. Nomor Kelompok: Group 1 Kelompok 4
2. Link Video Demo: 


## ERD
![ERD](https://gitlab.com/Firmanto/final-project/-/raw/master/backend-laravel/public/erd.jpg)

## Postman Documentation
- https://www.postman.com/syarif81/workspace/biodata/overview


## Langkah-langkah Git Clone project pertama kali:
1. buat folder kosong lalu masuk ke folder tersebut
2. git clone https://gitlab.com/Firmanto/final-project.git
3. masuk ke folder final-project lalu git checkout master
4. composer update --ignore-platform-reqs
5. composer install --ignore-platform-reqs
6. buat database kosong di phpmyadmin
7. rename .env.example jadi .env atau create file .env
8. sesuaikan isi .env dengan DB, Mailtrap Queue yang ada
- QUEUE_CONNECTION=database
- DB_CONNECTION=mysql
- DB_HOST=127.0.0.1
- DB_PORT=3306
- DB_DATABASE=namadatabase
- DB_USERNAME=root
- DB_PASSWORD=
- MAIL_MAILER=smtp
- MAIL_HOST=smtp.mailtrap.io
- MAIL_PORT=2525
- MAIL_USERNAME=isi-dengan-username-mailtrap-anda
- MAIL_PASSWORD=isi-dengan-password-mailtrap-anda
- MAIL_ENCRYPTION=tls
- MAIL_FROM_ADDRESS=isi-dengan-email-anda
9. php artisan key:generate
10. php artisan migrate:fresh
11. php artisan serve
12. php artisan queue:listen

## Langkah-langkah Git Pull project kedua dst:
1. git pull https://gitlab.com/Firmanto/final-project.git
2.  git checkout master

## Langkah-langkah Git Push
1. git pull
2. git add .
3. git commit -m "Isi Apa yang di Update"
4. git push -u origin master

## Menjalankan Project
1. php artisan serve
2. php artisan queue:listen