<?php

namespace App\Http\Controllers;

use App\Biodata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use App\Events\BiodataCreated;

class BiodataController extends Controller
{
    public function __construct() {
        return $this->middleware('auth:api')->only([
            'store',
            'update',
            'delete'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $biodata = Biodata::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'daftar biodata berhasil ditempilkan',
            'data' => $biodata
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required', 
            'umur' => 'integer|min:20',
            'tempat_lahir' => 'max:255',
            'tanggal_lahir' => 'nullable|date', 
            'alamat' => 'max:255', 
            'golongan_darah' => 'in:a,b,ab,o', 
            'pekerjaan' => 'in:pelajar,mahasiswa,karyawan', 
            'email' => 'required|email', 
            'foto' => 'url', 
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $biodata = Biodata::create([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'alamat' => $request->alamat,
            'golongan_darah' => $request->golongan_darah,
            'pekerjaan' => $request->pekerjaan,
            'email' => $request->email,
            'foto' => $request->foto,
            'created_by' => auth()->user()->id,
            'updated_by' => auth()->user()->id
        ]);

        //event(new BiodataCreated($biodata));

        return response()->json([            
            'success' => true,
            'message' => 'daftar Biodata berhasil ditambahkan',
            'data' => $biodata
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */




    public function show($id)
    {
        $biodata = Biodata::find($id)->first();
        return response()->json([            
            'success' => true,
            'message' => 'Biodata berhasil ditampilkan',
            'data' => $biodata
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required', 
            'umur' => 'integer|min:20',
            'tempat_lahir' => 'max:255',
            'tanggal_lahir' => 'nullable|date', 
            'alamat' => 'max:255', 
            'golongan_darah' => 'in:a,b,ab,o', 
            'pekerjaan' => 'in:pelajar,mahasiswa,karyawan', 
            'email' => 'required|email', 
            'foto' => 'url', 
        ]);

        $biodata = Biodata::find($id);
        $biodata->nama = $request->nama;
        $biodata->umur = $request->umur;
        $biodata->tempat_lahir = $request->tempat_lahir;
        $biodata->tanggal_lahir = $request->tanggal_lahir;
        $biodata->alamat = $request->alamat;
        $biodata->golongan_darah = $request->golongan_darah;
        $biodata->pekerjaan = $request->pekerjaan;
        $biodata->email = $request->email;
        $biodata->foto = $request->foto;
        $biodata->updated_by = auth()->user()->id;

        $biodata->save();

        return response()->json([            
            'success' => true,
            'message' => 'Daftar Biodata berhasil diupdate',
            'data' => $biodata
        ]);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $biodata = Biodata::find($id);
        $biodata->delete();

        return response()->json([            
            'success' => true,
            'message' => 'Biodata berhasil dihapus',
            'data' => $biodata
        ]);
    }
}
