<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Validator;
use App\User;
use App\OtpCode;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 400);
        };

        $otpCode = OtpCode::where('otp', $request->otp)->first();
        if(!$otpCode) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code Not Found',
            ], 404);
        }

        if($otpCode->valid_until < Carbon::now()) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code Expired',
            ], 404);
        }

        
        $user = $otpCode->user;
        //dd('done cek otp');
        $user->update([
            'email_verified_at' => Carbon::now()
        ]);

        $otpCode->delete();

        return response()->json([
            'success' => true,
            'message' => 'Verifikasi Berhasil',
            'data' => [
                'user' => $user,
            ]
        ]);

    }
}
